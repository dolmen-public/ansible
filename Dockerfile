FROM python:3-alpine as ansible

ARG UID=1000
ARG GID=1000

RUN apk --update add \
    sudo             \
    openssl          \
    ca-certificates  \
    sshpass          \
    openssh-client   \
    rsync            \
    bind-tools       \
    openssh-client   \
    git

RUN apk --update add --virtual \
    build-dependencies     \
    python3-dev            \
    libffi-dev             \
    openssl-dev build-base \
    && pip install --upgrade cffi ansible ansible-lint \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*

RUN pip install --upgrade \
    netaddr \
    jmespath

ENV ANSIBLE_BASE_FOLDER=/app/.ansible
RUN mkdir -p ${ANSIBLE_BASE_FOLDER} && chmod 777 ${ANSIBLE_BASE_FOLDER}

RUN addgroup -g ${GID} -S host_group \
    && adduser -u ${UID} -S host_user -G host_group

USER host_user

WORKDIR /app

COPY entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["ansible-playbook", "--version"]


