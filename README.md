# Ansible

Docker image ready to execute:

* [Ansible](https://www.ansible.com/) 

All ansible defaults variables relative to $HOME(~) are set up relative to workdir (/app)

## Run

```console
docker run --rm \
	-v "$(pwd):/app" \
	--volume $SSH_AUTH_SOCK:/ssh-agent \
	--env SSH_AUTH_SOCK=/ssh-agent \
	registry.gitlab.com/dolmen-public/ansible ansible-playbook --version
```


## Non-root host

This image provide a non-root user 1000:1000. If you want to use another, it's preferable to rebuild it with your gid/uid as arguments

```console
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t registry.gitlab.com/dolmen-public/ansible .
```
