#!/usr/bin/env sh
set -u

export ANSIBLE_ACTION_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/action:/usr/share/ansible/plugins/action
export ANSIBLE_BECOME_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/become:/usr/share/ansible/plugins/become
export ANSIBLE_CACHE_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/cache:/usr/share/ansible/plugins/cache
export ANSIBLE_CALLBACK_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/callback:/usr/share/ansible/plugins/callback
export ANSIBLE_CLICONF_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/cliconf:/usr/share/ansible/plugins/cliconf
export ANSIBLE_COLLECTIONS_PATHS="${ANSIBLE_BASE_FOLDER}"/collections:/usr/share/ansible/collections
export ANSIBLE_CONNECTION_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/connection:/usr/share/ansible/plugins/connection
export ANSIBLE_DOC_FRAGMENT_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/doc_fragments:/usr/share/ansible/plugins/doc_fragments
export ANSIBLE_FILTER_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/filter:/usr/share/ansible/plugins/filter
export ANSIBLE_HTTPAPI_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/httpapi:/usr/share/ansible/plugins/httpapi
export ANSIBLE_INVENTORY_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/inventory:/usr/share/ansible/plugins/inventory
export ANSIBLE_LOCAL_TEMP="${ANSIBLE_BASE_FOLDER}"/tmp/
export ANSIBLE_LOOKUP_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/lookup:/usr/share/ansible/plugins/lookup
export ANSIBLE_LIBRARY="${ANSIBLE_BASE_FOLDER}"/plugins/modules:/usr/share/ansible/plugins/modules
export ANSIBLE_MODULE_UTILS="${ANSIBLE_BASE_FOLDER}"/plugins/module_utils:/usr/share/ansible/plugins/module_utils
export ANSIBLE_NETCONF_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/netconf:/usr/share/ansible/plugins/netconf
export ANSIBLE_PERSISTENT_CONTROL_PATH_DIR="${ANSIBLE_BASE_FOLDER}"/pc
export ANSIBLE_ROLES_PATH="${ANSIBLE_BASE_FOLDER}"/roles:"${ANSIBLE_BASE_FOLDER}"/galaxy-roles:/usr/share/ansible/roles:/etc/ansible/roles
export ANSIBLE_SSH_CONTROL_PATH_DIR="${ANSIBLE_BASE_FOLDER}"/cp
export ANSIBLE_STRATEGY_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/strategy:/usr/share/ansible/plugins/strategy
export ANSIBLE_TERMINAL_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/terminal:/usr/share/ansible/plugins/terminal
export ANSIBLE_TEST_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/test:/usr/share/ansible/plugins/test
export ANSIBLE_VARS_PLUGINS="${ANSIBLE_BASE_FOLDER}"/plugins/vars:/usr/share/ansible/plugins/vars

eval "$@"
